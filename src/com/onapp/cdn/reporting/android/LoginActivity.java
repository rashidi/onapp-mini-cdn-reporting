/**
 * 
 */
package com.onapp.cdn.reporting.android;

import org.xmlrpc.android.XMLRPCClient;
import org.xmlrpc.android.XMLRPCException;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * @author shidi
 * @version 1.0
 * @since 1.0
 */
public class LoginActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		
		final Button btnLogin = (Button) findViewById(R.id.login);
		btnLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final String username = ((EditText) findViewById(R.id.username)).getText().toString();
				final String password = ((EditText) findViewById(R.id.password)).getText().toString();
				
				if ((username != null && password != null) && (username.length() > 3 && password.length() > 5)) {
					String xmlrpcUrl = getString(R.string.xmlrpc_url_core);
					XMLRPCClient client = new XMLRPCClient(xmlrpcUrl, username, password);
					try {
						Object o = client.call("auth.authenticate", username, password);
						
						if (Boolean.TRUE.equals(o)) {
							
							SharedPreferences settings = getSharedPreferences(getString(R.string.credentials), 0);
							SharedPreferences.Editor editor = settings.edit();
							
							editor.putString(getString(R.string.username), username);
							editor.putString(getString(R.string.password), password);
							editor.commit();
							
							Intent intent = new Intent(getApplicationContext(), MainActivity.class);
							startActivity(intent);
						}
					} catch (XMLRPCException e) {
						throw new RuntimeException(e);
					}
				}
			}
		});
	}
}

