/**
 * 
 */
package com.onapp.cdn.reporting.android;

import static java.lang.reflect.Array.get;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xmlrpc.android.XMLRPCClient;
import org.xmlrpc.android.XMLRPCException;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

/**
 * @author shidi
 * @version 1.0
 * @since 1.0
 */
public class EntityActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.content_entity);
		
		SharedPreferences settings = getSharedPreferences(getString(R.string.credentials), 0);
		
		String username = settings.getString(getString(R.string.username), null);
		String password = settings.getString(getString(R.string.password), null);
		
		try {
			Object o = execute(username, password);
			
			String publishers = String.valueOf(get(o, 0));
			String edges = String.valueOf(get(o, 1));
			String resources = String.valueOf(get(o, 2));
			String locations = String.valueOf(get(o, 3));
			
			final TextView publisherTv = (TextView) findViewById(R.id.textview_publisher);
			publisherTv.setText(publishers.substring(publishers.indexOf("=") + 1).replace("}", "") + " " + getString(R.string.active));
			
			final TextView edgeTv = (TextView) findViewById(R.id.textview_edge);
			edgeTv.setText(edges.substring(edges.indexOf("=") + 1).replace("}", "") + " " + getString(R.string.active));
			
			final TextView resourceTv = (TextView) findViewById(R.id.textview_resource);
			resourceTv.setText(resources.substring(resources.indexOf("=") + 1).replace("}", "") + " " + getString(R.string.active));

			final TextView locationTv = (TextView) findViewById(R.id.textview_location);
			locationTv.setText(locations.substring(locations.indexOf("=") + 1).replace("}", "") + " " + getString(R.string.active));
			
		} catch (XMLRPCException e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("serial")
	private Object execute(String username, String password) throws XMLRPCException {
		String xmlrpcUrl = getString(R.string.xmlrpc_url_core);
		
		XMLRPCClient client = new XMLRPCClient(xmlrpcUrl, username, password);
		List<Map<String, Object>> helper = new ArrayList<Map<String,Object>>();

		Map<String, Object> userMap = new HashMap<String, Object>();
		userMap.put("method", "users.count");
		userMap.put("args", new ArrayList<Map<String, Object>>() {{ 
			add(new HashMap<String, Object>() {{ put("status", "ACTIVE"); put("role", "PUBLISHER"); }});
		}});
		
		Map<String, Object> edgeMap = new HashMap<String, Object>();
		edgeMap.put("method", "edges.count");
		edgeMap.put("args", new ArrayList<Map<String, Object>>() {{ 
			add(new HashMap<String, Object>() {{ put("status", "ACTIVE"); }});
		}});

		Map<String, Object> resourceMap = new HashMap<String, Object>();
		resourceMap.put("method", "resources.count");
		resourceMap.put("args", new ArrayList<Map<String, Object>>() {{ 
			add(new HashMap<String, Object>() {{ put("status", "ACTIVE"); }});
		}});
		
		Map<String, Object> locationMap = new HashMap<String, Object>();
		locationMap.put("method", "locations.count");
		locationMap.put("args", new ArrayList<Map<String, Object>>() {{ 
			add(new HashMap<String, Object>() {{ put("status", "ACTIVE"); }});
		}});
		
		helper.add(userMap);
		helper.add(edgeMap);
		helper.add(resourceMap);
		helper.add(locationMap);
		
		return client.call("helper.request", username, password, helper);
	}
}
