/**
 * 
 */
package com.onapp.cdn.reporting.android;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.xmlrpc.android.XMLRPCClient;
import org.xmlrpc.android.XMLRPCException;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * @author shidi
 * @version 1.0
 * @since 1.0
 */
public class UsageActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.content_usage);

		final TextView directTv = (TextView) findViewById(R.id.txt_usage_direct);
		directTv.setText(String.valueOf(this.getTotalDirectTraffic()));
		
		final TextView peerTv = (TextView) findViewById(R.id.txt_usage_peer);
		peerTv.setText(String.valueOf(this.getTotalPeerTraffic()));
	}
	
	
	private Object getUsage(String served, String summary ){
		
		int frequency = 0;
		int hours = 5040; // For 30 Days in hours
		Object result;
		
		Calendar calendar = Calendar.getInstance();
		
		Map<String, Object> filter = new HashMap<String, Object>();		
		
		if(served == "DIRECT" || served == "PEERED"){
			filter.put("served", served);
		}
		
		if(summary == "DAY"){
			hours = 24;
			frequency = 3600;
		}else if(summary == "WEEK"){
			hours = 168;
			frequency = 25200;
		}else if(summary == "MONTH"){
			hours = 5040;
			frequency = 86400;
		}
		
		filter.put("endDate", calendar.getTime());
		
		calendar.add(Calendar.HOUR_OF_DAY, -hours);
		filter.put("startDate",  calendar.getTime());
		

		filter.put("role", "OPERATOR");
		if(frequency != 0){
			filter.put("frequency", frequency);
			filter.put("groupBy", "location");
		}else{
			filter.put("total", true);
		}
		
		try {
			XMLRPCClient client = new XMLRPCClient(getString(R.string.xmlrpc_url_stats));
			result = client.call("stats.getUsage","oceanus", "8ucker", filter);
		} catch (XMLRPCException e) {
			throw new RuntimeException(e);
		}
		
		return result;
	}
	
	/**
	 * Get Direct Traffic
	 * @return
	 */
	private float getTotalDirectTraffic(){
		@SuppressWarnings("unchecked")
		Map<String, Object> map = (Map<String, Object>) this.getUsage("DIRECT", "NONE");
		return Math.round((Double) map.get("result"));
	}

	
	/**
	 * Get Direct Traffic
	 * @return
	 */
	private float getTotalPeerTraffic(){
		@SuppressWarnings("unchecked")
		Map<String, Object> map = (Map<String, Object>) this.getUsage("PEERED", "NONE");
		return Math.round((Double) map.get("result"));
	}
}
