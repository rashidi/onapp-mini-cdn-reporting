package com.onapp.cdn.reporting.android;

import android.app.TabActivity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.TabHost;

/**
 * 
 * @author shidi
 * @version 1.0
 * @since 1.0
 */
public class MainActivity extends TabActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Resources res = getResources();
        TabHost tabHost = getTabHost();
        TabHost.TabSpec spec;
        
        Intent intent;
        
        intent = new Intent().setClass(this, EntityActivity.class);
        spec = tabHost.newTabSpec(getString(R.string.tab_entity))
        		.setIndicator(getString(R.string.tab_entity), 
        				res.getDrawable(R.drawable.tab_entity))
        		.setContent(intent);
        tabHost.addTab(spec);
        
        intent = new Intent().setClass(this, UsageActivity.class);
        spec = tabHost.newTabSpec(getString(R.string.tab_usage))
        		.setIndicator(getString(R.string.tab_usage),
        				res.getDrawable(R.drawable.tab_usage))
        		.setContent(intent);
        tabHost.addTab(spec);
        
        tabHost.setCurrentTab(1);
    }
}